// Server Dependencies
const express = require('express')
const mongoose = require('mongoose')
require('dotenv').config()
const taskRoutes = require('./routes/taskRoutes')

// MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.ucfa5iz.mongodb.net/${process.env.DATABASE_NAME}?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
	})

let db = mongoose.connection

db.on('error', () => console.log('Something went wrong!'))

db.once('open', () => console.log('Connected to MongoDB!'))

// Server Setup
const app = express()
const port = process.env.PORT

// Middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Routes
app.use('/tasks', taskRoutes)

// Server Listening
app.listen(port, () => console.log(`Server is running at localhost: ${port}`))